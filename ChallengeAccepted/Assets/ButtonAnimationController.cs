﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimationController : MonoBehaviour
{

    public void HideMenuButton()
    {
        gameObject.GetComponent<Animation>().Play("HideMenuButton");
    }

    public void ShowMenuButton()
    {
        gameObject.GetComponent<Animation>().Play("ShowMenuButton");
    }

}
