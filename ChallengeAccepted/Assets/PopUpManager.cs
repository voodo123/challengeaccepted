﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpManager : MonoBehaviour
{
    [SerializeField] ButtonInputFieldManager _ButtonInputFieldPrefab;

    [SerializeField] private bool _IsOpen;
    public bool IsOpen { get { return _IsOpen; } set { _IsOpen = value; } }

    [SerializeField] public List<ButtonInputFieldManager> ListFromContent = new List<ButtonInputFieldManager>();

    public static PopUpManager THIS;

    void Awake()
    {
        THIS = this;
    }

    public void GenerateMyInputFields(List<GameObject> ListaOfButtons)
    {
        //Remove gameobjects from Scene and PopUpSection list.
        DestroyElementsInList();

        InsertAndInstantiateInputFieldsToList();

    }

    public void DestroyElementsInList()
    {
        if (ListFromContent.Count > 0)
        {
            foreach (ButtonInputFieldManager e in PopUpManager.THIS.ListFromContent)
            {
                Destroy(e.gameObject);
            }
        }

        ListFromContent.RemoveAll(item => item.gameObject);

    }

    public void InsertAndInstantiateInputFieldsToList()
    {
        for (int i = 0; i < ContentGenerator.THIS.listOfButoons.Count; i++)
        {
            if (ListFromContent.Count < ContentGenerator.THIS.listOfButoons.Count)
            {
                ListFromContent.Add(Instantiate(_ButtonInputFieldPrefab, transform));
            }
            else
            {
                break;
            }
        }
    }

    public void ApplyNameButtons(List<GameObject> ButtonList)
    {
        int i = 0;
        foreach (ButtonInputFieldManager input in ListFromContent)
        {
            input.LabelName.text = ButtonList[i].GetComponent<ButtonController>().ButtonName.text;
            i++;
        }
    }

}
