﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    [SerializeField] InputField _MyInput;

    private void OnEnable()
    {
        _MyInput.text = "0";
    }

    public void OnValueChange()
    {
        int number;

        bool success = Int32.TryParse(_MyInput.text,out number);

        if (success)
        {
            _MyInput.text = number.ToString();
        }
        else
        {
            _MyInput.text = "0";
        }
            
    }
}
