﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentGenerator : MonoBehaviour
{
    [SerializeField] ButtonAnimationController _menuButton;
    [SerializeField] GameObject _ButtonPrefab, _closeMenuButton, _SubmitButton, _LoadData, _ClearContent, _CleanTable;
    [SerializeField] InputField _InputField;
    [SerializeField] Animation _Animation;

    public static ContentGenerator THIS;
	private int sizeOfContent;


    private bool isEmpty = true;
    public List<GameObject> listOfButoons = new List<GameObject>();


    private void Awake()
    {
        THIS = this;
    }


    public void RemoveAndInstantiateButtonsInsideMenu()
    {
        PlayerPrefs.DeleteAll();
        //PlayAnimationButton
        _SubmitButton.GetComponent<Animation>().Play("SubmitButton");

        //Number of buttons inside menu
        sizeOfContent = int.Parse(_InputField.text);
        PlayerPrefs.SetInt("NumberOfButtons", sizeOfContent);


        //Remove gameobjects from Scene.
        //REMOVE ALL OBJECT FROM LIST SO LIST WILL HAVE SIZE 0 
        CleanTable();

        //INSTANIATE ELEMENTS INTO SCENE AND LIST
        InsertAndInstantiateButtonsToList(sizeOfContent);

        //INSTANIATE INPUT FIELDS FOR EVERY BUTTON
        PopUpManager.THIS.GenerateMyInputFields(listOfButoons);

        //UPDATE LABELS NAME TO BUTTONS REFERENCE NAME
        PopUpManager.THIS.ApplyNameButtons(listOfButoons);
     
    }

    public void ClearDataAll()
    {
        RefreshNumberOfElements();
        PlayerPrefs.DeleteAll();
        CleanTable();
        PopUpManager.THIS.DestroyElementsInList();
        _InputField.text = listOfButoons.Count.ToString();


    }

    public void CleanTable()
    {
        DestroyElementsInList();
        listOfButoons.RemoveAll(item => item);
        PopUpManager.THIS.DestroyElementsInList();
        _InputField.text = "0";
    }

    public void RefreshNumberOfElements()
    {
        if (PlayerPrefs.HasKey("NumberOfButtons"))
            _InputField.text = PlayerPrefs.GetInt("NumberOfButtons").ToString();
    }

    public void Start()
    {
        int loadDataContentSize = 0;
        if (PlayerPrefs.HasKey("NumberOfButtons"))
        {
            loadDataContentSize = PlayerPrefs.GetInt("NumberOfButtons");
            _InputField.text = loadDataContentSize.ToString();
        }

        DestroyElementsInList();

        listOfButoons.RemoveAll(item => item);

        InsertAndInstantiateButtonsToList(loadDataContentSize);

        PopUpManager.THIS.GenerateMyInputFields(listOfButoons);

        PopUpManager.THIS.ApplyNameButtons(listOfButoons);


    }

    public void DestroyElementsInList()
    {
        if (listOfButoons.Count > 0)
        {
            foreach (GameObject e in listOfButoons)
            {
                Destroy(e);
            }
        }
    }

    public void InsertAndInstantiateButtonsToList(int size)
    {
		DestroyElementsInList();

		for (int i = 0; i < size; i++)
        {
            listOfButoons.Add(Instantiate(_ButtonPrefab, transform));
            listOfButoons[i].GetComponent<ButtonController>().ButtonName.text = "Przycisk " + (i + 1);
            listOfButoons[i].GetComponent<ButtonController>().index = i;
			listOfButoons[i].SetActive(false);


		}
        _InputField.text = size.ToString();
    }

	IEnumerator LoadButtons(int size)
	{
		for (int i = 0; i < size; i++)
		{
			listOfButoons[i].SetActive(true);
			yield return new WaitForSeconds(0.25f);
		}
	}


	public void OpenMenu()
    {
		_menuButton.HideMenuButton();
        _Animation.Play("MenuAnimate");
        _closeMenuButton.GetComponent<Animation>().Play("CloseMenuButtonShow");
        _InputField.GetComponent<Animation>().Play("HideMenuButton");
        _SubmitButton.GetComponent<Animation>().Play("HideMenuButton");
        _LoadData.GetComponent<Animation>().Play("HideMenuButton");
        _ClearContent.GetComponent<Animation>().Play("HideMenuButton");
        _CleanTable.GetComponent<Animation>().Play("HideMenuButton");
		StartCoroutine(LoadButtons(int.Parse(_InputField.text)));
	}

    public void CloseMenu()
    {
        _Animation.Play("CloseAnimate");
        _closeMenuButton.GetComponent<Animation>().Play("CloseMenuButtonHide");
        _InputField.GetComponent<Animation>().Play("ShowMenuButton");
        _SubmitButton.GetComponent<Animation>().Play("ShowMenuButton");
        _LoadData.GetComponent<Animation>().Play("ShowMenuButton");
        _ClearContent.GetComponent<Animation>().Play("ShowMenuButton");
        _CleanTable.GetComponent<Animation>().Play("ShowMenuButton");
        _menuButton.ShowMenuButton();

        foreach (ButtonInputFieldManager e in PopUpManager.THIS.ListFromContent)
        {
            e.gameObject.SetActive(false);
        }
    }
}
