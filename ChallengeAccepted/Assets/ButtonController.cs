﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
	[SerializeField] Animation ButtonLoading;

    [SerializeField] private int _Index;
    public int index { get { return _Index; } set { _Index = value; } }

    [SerializeField] private Text _ButtonName;
    public Text ButtonName { get { return _ButtonName; } set { _ButtonName = value; } }

    private int indexOfInputField;


	private void OnEnable()
	{
		GetComponentsInChildren<Animation>()[1].Play("ButtonLoading");
		
	}

	public void OpenInputField()
    {
        CloseOtherInputField();
		PopUpManager.THIS.ListFromContent[index].gameObject.SetActive(true);
        PopUpManager.THIS.ListFromContent[index].index = index;
        PopUpManager.THIS.ListFromContent[index].Content.text = PlayerPrefs.GetString(index.ToString());

        //PopUpManager.THIS.IsOpen = true;
    }

    public void CloseOtherInputField()
    {
        foreach (ButtonInputFieldManager e in PopUpManager.THIS.ListFromContent)
        {
            e.gameObject.SetActive(false);
        }
    }

}
