﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInputFieldManager : MonoBehaviour
{

    [SerializeField] Text _LabelName;
    [SerializeField] InputField _Content;
    [SerializeField] public int index;

    public static ButtonInputFieldManager THIS;

    public Text LabelName
    {
        get { return _LabelName; }
        set { _LabelName = value; }
    }

    private void Awake()
    {
        THIS = this;
    }

    public InputField Content
    {
        get { return _Content; }
        set { _Content = value; }
    }

    public void SaveDataToPlayerPrefOnChange()
    {
        Debug.Log(index.ToString() + " " + Content.text);
        PlayerPrefs.SetString(index.ToString(), Content.text);
    }

    public void CloseInputField()
    {
        gameObject.GetComponent<Animation>().Play("ButtonInputFieldHide");
        PopUpManager.THIS.IsOpen = false;
    }

    private void OnEnable()
    {
        gameObject.GetComponent<Animation>().Play("ButtonInputFieldShow");

    }

}
